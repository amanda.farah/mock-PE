## Models
Put the names of the gwpopulation models here. For example:
```python
gwpopulation.models.mass.SinglePeakSmoothedMassDistribution,
gwpopulation.models.redshift.PowerLawRedshift
```

## Hyperparameters
Put the hyperparameter values here. For example:
```python
dict(
    alpha=3.14,
    mmin=4.56,
    delta_m=5.96,
    bump_fraction=0.0,
    mpp=35.,
    sigpp=5.,
    mmax=81.08,
    lam=0,
    beta=1.70,
    lamb=2.97
)
```

## Other options
**Number of events:** 

**Number of samples per event:**

**Number of realizations** (how many sets of mock PE to make from this population):



/label ~"Population Request" 
