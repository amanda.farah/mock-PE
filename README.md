![](GWMockCatlogo.jpg)

Welcome to `GWMockCat`, the **G**ravitational **W**ave **Mock** **Cat**alog generator!

## Description
This is a lightweight python-based package to compute mock parameter estimation (PE) posteriors for gravitational wave events and generate synthetic catalogs.

Planned features can be found in the [Issues](https://git.ligo.org/amanda.farah/mock-PE/-/issues), and users should feel free to request features or sets of mock PE.

A description of available mock PE at Tag [RP_MDC_0](https://git.ligo.org/amanda.farah/mock-PE/-/tags/RP_MDC_0) can be found in the [slides](https://git.ligo.org/amanda.farah/mock-PE/-/blob/main/Synthetic%20Event%20Posteriors%20R%2BP%20Mock%20Data%20Challenge.pdf).

## Instructions
### Installation
Users should clone the repo and navigate into it using 
```
git clone https://git.ligo.org/amanda.farah/GWMockCat.git
cd GWMockCat
```
Then, the module is pip installable via
```
pip install .
```
You can test that things have been installed correctly via
```
generate_mock_cats -h
```

### Usage
See what settings are available to change via 
```
generate_mock_cats -h
```
You can change these by creating a config file (a minimal example is provided called `example_config.ini`) and running it via

```
generate_mock_cats -c config.ini
```
### More details
The modules `posterior_utils`, `vt_utils`, and `cosmo_utils` house functions that allow the user to compute posteriors with realistic scatter and error given true parameters for events. They also estimate the observed SNR of an event given its true parameters.

Likelihoods are estimated in detector-frame chirp mass, symmetric mass ratio, sky angle, and signal-to-noise ratio (SNR).
These parameters are correlated through their dependence on SNR, but beyond that no parameter correlation is assumed. 
Then, all samples are transformed to detector-frame component masses and luminosity distance, resulting in somewhat realistic degeneracies between component masses. 
Redshifts and source frame masses are also computed for convenience, assuming Planck 2015 cosmology.
A full description is availabe in Section 2.2 of [this paper](add link).

The excecuteable generates several populations of events according to a user-specified model, as well as a semianalytic injection set to accompany it.
The model must be in `gwpopulation`, user-defined models are not supported at this time, but MRs are welcome.

## Words of caution
There is a known issue with the priors returned by the `transforms` module. We recommend using the "lalprior" and then computing the priors yourself, similarly to [this example](https://git.ligo.org/RatesAndPopulations/gwpopulation_pipe/-/blob/master/gwpopulation_pipe/data_collection.py#L107).

Working with symmetric mass ratio can be annoying and, depending on your popualtion model, you may find too few effective samples at high (symmetric) mass ratios. We are working on a fix to this that uses an exponential interim prior in symmetric mass ratio, but it is not ready yet. In the meantime, please be cautios of this behavior and don't take mass ratio results too seriously.

## Citing
If you use this package for a scientific publication, please cite (insert bibentry here).


This code is based largely on code written by Maya Fishbach and Will Farr, which can be found in [this repo](https://git.ligo.org/maya.fishbach/o3-population-analyses/-/tree/master), and the general framework is described in the appendix of [this paper](https://ui.adsabs.harvard.edu/abs/2020ApJ...891L..31F/abstract).
Please consider citing that paper as well.

## Authors and Contributors
- Amanda Farah (amanda.farah@ligo.org)
- Maya Fishbach
- Bruce Edelman
- Mike Zevin
- Jose Maria Ezquiaga
