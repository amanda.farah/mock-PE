from setuptools import setup, find_packages

VERSION = '0.0.1'
DESCRIPTION = 'Synthetic GW event posterior generator'
LONG_DESCRIPTION = 'A lightweight package to quickly generate synthetic mass'
'and distance posteriors for gravitational wave events and catalogs'
'from user-defined populations.'

# Setting up
setup(
        name="GWMockCat",
        version=VERSION,
        author="Amanda Farah",
        author_email="<afarah@uchicago.edu>",
        description=DESCRIPTION,
        long_description=LONG_DESCRIPTION,
        packages=find_packages(),
        install_requires=['xarray','gwpopulation<1.0','numpy','h5py','scipy<1.14','astropy','configargparse'],
        entry_points={"console_scripts":[
            "generate_mock_cats=GWMockCat.main:main"]},
        keywords=['python', 'gravitational waves'],
        classifiers= [
            "Development Status :: 3 - Alpha",
            "Intended Audience :: Education",
            "Programming Language :: Python :: 3",
            "Operating System :: OS Independent",
        ]
)
