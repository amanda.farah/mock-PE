__author__ = "Maya Fishbach"
from GWMockCat import vt_utils as vt
import numpy as np
import xarray as xr
from astropy import units as u
from scipy.stats import truncnorm

#uncert_default = {'threshold_snr': 11, 'mc': 0.007, 'Theta': 0.15*1.4, 'eta':
#                  0.04, 'snr':1.0} #try to increase mass uncertainties
uncert_default = {'threshold_snr': 8, 'snr': 1.0, 'mc': 0.08, 'Theta': 0.15*1.4, 'eta': 0.022}

def mchirp(m1,m2):
    return (m1*m2)**0.6/(m1+m2)**0.2

def etafunc(m1,m2):
    return (m1*m2)/(m1+m2)**2

def m1m2_from_mceta(mc,eta):
    mtot = mc/eta**0.6
    m1m2 = eta*mtot**2
    m2 = (mtot-np.sqrt(mtot**2-4*m1m2))/2.0
    m1 = (mtot+np.sqrt(mtot**2-4*m1m2))/2.0
    return m1, m2

def dm1m2_dMceta(m1,m2): #Jacobian transformation
    eta = etafunc(m1,m2)
    return (m1+m2)**2/((m1-m2)*eta**0.6)

def ddL_drho(osnr_gpc_func,m1det,m2det,rho,theta):
    return osnr_gpc_func(m1det,m2det,grid=False)/rho**2*theta

def chirp_mass_scaling(mc, uncert):
    return uncert['mc_shift']+ np.power((mc*uncert['mc_coeff']),uncert['mc_power'])

def mc_sigma(rho_obs,mc,uncert):
    term1 = uncert['threshold_snr']/rho_obs*uncert['mc']
    term2 = np.power(log(mc)*uncert['mc_coeff'],uncert['mc_power'])
    term3 = uncert['mc_shift']
    sigma= term1 + term2 + term3
    sigma_capped = where(sigma>1e-5, sigma,1e-5)
    return sigma_capped

def generate_detected_from_true_list(m1, m2, z, osnr_interp, cosmo_dict, rng, osnr_interp_dist = u.Gpc, t = None, uncert=uncert_default):
    if t is None:
        t = vt.draw_thetas(len(m1),rng)
    m1det = m1*(1+z)
    m2det = m2*(1+z)
    dl = cosmo_dict['dL'](z)*((cosmo_dict['dist_Mpc']*u.Mpc))
    rho_opt = osnr_interp(m1det, m2det, grid=False)/dl.to(osnr_interp_dist).value
    rho_true = rho_opt * t
    rho_obs = rho_true+uncert['snr']*rng.normal(size=len(m1))
    det_sel = rho_obs > uncert['threshold_snr']
    return {'m1': m1[det_sel], 'm2': m2[det_sel], 'z': z[det_sel],
            'theta':t[det_sel],
            'lum_dist': dl[det_sel].value, 'z': z[det_sel],
            'rho_obs': rho_obs[det_sel], 'rho_opt':rho_opt[det_sel], 
            'detected_index': np.array(np.arange(len(m1)))[det_sel]}

def generate_obs_from_true_list(m1t, m2t, zt, osnr_interp, cosmo_dict, rng,
                                osnr_interp_dist = u.Gpc, PEsamps = None,
                                num_evs=np.inf,t = None, uncert=uncert_default):
    detected_dict = generate_detected_from_true_list(m1t, m2t, zt, osnr_interp,
                                                     cosmo_dict, rng,
                                                     osnr_interp_dist, t,uncert)
    # source frame 
    m1, m2, z = detected_dict['m1'], detected_dict['m2'], detected_dict['z']
    mc = mchirp(m1,m2)*(1+z) # detector frame
    eta = etafunc(m1, m2)
    rho_obs = detected_dict['rho_obs']
    smc = uncert['threshold_snr']/rho_obs*uncert['mc']
#    smc = mc_sigma(rho_obs,mc,uncert)
    mcobs = rng.lognormal(mean=np.log(mc), sigma=smc)
    seta = uncert['threshold_snr']/rho_obs*uncert['eta']
    # should maybe change below to not be truncated
    etaobs = eta+seta*truncnorm.rvs((0.0-eta)/seta,(0.25-eta)/seta,size=len(m1),random_state=rng)
    st = uncert['threshold_snr']/rho_obs*uncert['Theta']
    theta = detected_dict['theta']
    tobs = theta+st*truncnorm.rvs((0.0-theta)/st, (1.0-theta)/st, size=len(m1),random_state=rng)
    if PEsamps is None:
        return {'Mc_det_obs': mcobs, 'eta_obs':etaobs, 'rho_obs': rho_obs,
                't_obs': tobs}, detected_dict
    else:
        num_evs = min(len(m1),num_evs)
        param_names = ['Mc_det_samps','eta_samps',
                       'rho_samps','theta_samps']
        event_data=xr.Dataset(coords={'parameter':param_names,'PE_sample':np.arange(PEsamps)})
        for i in range(num_evs):
            mc_samps = rng.lognormal(mean=np.log(mcobs[i]),sigma=smc[i],size=PEsamps)
            eta_mean = eta[i]+seta[i]*rng.normal()
            eta_samps = truncnorm((0.0-eta_mean)/seta[i],(0.25-eta_mean)/seta[i],
                                  loc=eta_mean,
                                  scale=seta[i]
                                  ).rvs(size=PEsamps,random_state=rng)
            t_mean = theta[i]+st[i]*rng.normal()
                                 
            theta_samps = truncnorm((0.0-t_mean)/st[i], (1.0-t_mean)/st[i],
                                    loc=t_mean,scale=st[i]
                                   ).rvs(size=PEsamps,random_state=rng)
            #rho_samps = rho_obs[i]+uncert['snr']*rng.normal(size=PEsamps)
            rho_samps = truncnorm((0.0-rho_obs[i])/uncert['snr'], np.inf,
                                  loc=rho_obs[i],scale=uncert['snr']
                                 ).rvs(size=PEsamps,random_state=rng)
            #print('rho_samps: ',rho_obs[i], rho_samps)
            stacked_samples = np.vstack([
                mc_samps,eta_samps,rho_samps,theta_samps])
            event_data=event_data.assign({f'event_{i}':
                                          xr.DataArray(stacked_samples,dims=['parameter','PE_sample'])})
        return event_data, detected_dict

