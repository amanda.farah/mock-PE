import configargparse
import yaml

parser = configargparse.ArgParser(
    config_file_parser_class=configargparse.ConfigparserConfigFileParser
)
# general arguments
parser.add('-c', '--my-config', required=True, is_config_file=True, help='config file path')
parser.add('-v', '--verbose', help='verbose', action="store_true")
parser.add('--output_path',default='out',
           help='absolute path for where to put the files that are created.')
parser.add('--run_label',required=True,
           help='what to label the files that are created.')
parser.add('--pesummary_format',
           help='whether to put results in a format similar to PEsummary',
           type=bool)
parser.add('--num_events',required=True, type=int, default=100,
           help='how many events in a catalog')
parser.add('--num_realizations',type=int,default=1,help='how many catalogs')
parser.add('--random_seed',type=int,default=None,help="random seed for use in numpy's random number generator")

# global rate settings
parser.add('--snr_interpolator_file',
           default='sensitivity/optimal_snr_aligo_O3actual_L1.h5',
           help='path to pre-saved optimal SNR interpolation file.'
           'If running outside of this repo, should make this an absolute path.')
parser.add('--max_redshift',type=float,default=3.0,help='maximum redshift')
parser.add('--threshold_snr',default=8,help='single-IFO SNR to threshold on.')

# population model settings
parser.add('--mass_model',
           default="SinglePeakSmoothedMassDistribution",
           help='mass distribution to be imported from gwpopulation.models.mass'
          )
parser.add('--redshift_model',
           default="PowerLawRedshift",
           help='redshift distribution to be imported from'
           'gwpopulation.models.redshift'
          )
parser.add('--hyperparameters',
           type=yaml.safe_load,
           default=None,
           help='hyperparameters for all distributions, must be in '
                 'dictionary format, i.e. {key:"value"}'
          )
# event-level settings
parser.add('--num_samples_per_event',type=int, default=5000,
           help='how many posterior samples to generate per event')
parser.add('--event_params',default='sourceframem1qz',
           help='what parameterization to output samples in. '
           'options are "detectorframem1m2z","detectorframeMcetarho", '
           '"sourceframem1qz", and "all". The prior is returned in terms '
           'of the parameters specified here.'
          )
parser.add('--sampling_prior',default='lalprior',
           help='what prior the samples should have. '
           'options are "lalprior" and "original". '
           '"lalprior" is uniform in detector frame component masses and '
           'proportional to luminosity distance squared. '
           '"original" is uniform in detector frame chirp mass, symmetric'
           ' mass ratio, optimal SNR, and sky angle. '
           'The prior is returned in terms '
           'of the parameters specified by --event_params.'
          )
parser.add('--SNR_uncert', default=1, type=float,
           help='standard deviation of observed SNR samples per-event.')
parser.add('--mc_uncert', default=0.08, type=float,
           help='standard deviation of observed log detector frame chirp mass'
           'samples per-event, when observed SNR=1.')
parser.add('--Theta_uncert', default=0.21, type=float,
           help='standard deviation of observed sky angle'
           'samples per-event, when observed SNR=1.')
parser.add('--eta_uncert', default=0.022, type=float,
           help='standard deviation of observed symmetric mass ratio'
           'samples per-event, when observed SNR=1.')

# injection parameters
parser.add('--create_injs',
           help='whether to create a corresponding injection set',
           action='store_true')

parser.add('--inj_mmin', default=1,
           help='minimum secondary mass of injection set',
           type=float)
parser.add('--inj_mmax', default=200,
           help='maximum primary  mass of injection set',
           type=float)
parser.add('--inj_alpha', default=2.35,
           help='primary mass spectral index for injection set',
           type=float)
parser.add('--inj_beta', default=1.7,
           help='mass ratio sepctral index for injection set',
           type=float)
parser.add('--inj_lamb', default=2.7,
           help='redshift sepctral index for injection set',
           type=float)
parser.add('--num_injections', default=int(2e7),
           help='total number of injections to generate',
           type=float)
