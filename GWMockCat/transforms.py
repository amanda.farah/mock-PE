__author__="Amanda Farah"

from GWMockCat.vt_utils import interpolate_optimal_snr_grid, draw_thetas
from GWMockCat import cosmo_utils
from GWMockCat.parser import parser
from scipy import stats
from scipy.interpolate import interp1d
import numpy as np
import xarray as xr

args = parser.parse_args()

cosmo_dict = cosmo_utils.interp_cosmology()
maximum_redshift=args.max_redshift
osnr_interp, reference_dist = interpolate_optimal_snr_grid(
    fname=args.snr_interpolator_file)
# variable transforms

## general PDFs for original prior
def original_prob(x1,x2,x3,x4,bounds1=(0,1),bounds2=(0,1),bounds3=(0,1),bounds4=(0,1)):
    """ original (flat) prior """
    return stats.uniform(loc=bounds1[0],scale=bounds1[1]-bounds1[0]).pdf(x1) \
        * stats.uniform(loc=bounds2[0],scale=bounds2[1]-bounds2[0]).pdf(x2) \
        * stats.uniform(loc=bounds3[0],scale=bounds3[1]-bounds3[0]).pdf(x3)\
        * stats.uniform(loc=bounds4[0],scale=bounds4[1]-bounds4[0]).pdf(x4)


def transformed_prob(y1,y2,y3,y4,*jacobians, **f_kwargs):
    J =1
    for jac in jacobians:
        J *= jac(y1,y2,y3,y4)
    f = original_prob(v1(y1,y2,y3,y4), v2(y1,y2,y3,y4), v3(y1,y2,y3,y4), v4(y1,y2,y3,y4), **f_kwargs)
    return np.abs(J) * f

## general PDFs for lalprior
lalprior=cosmo_utils.lalprior_func(cosmo_dict)
zs = np.linspace(0, maximum_redshift * 1.01, 1000)
pz = lalprior(zs)
norm = np.trapz(pz,zs)

def ptheta_func():
    # this passes in a separate random seed than the rest of the code because this function is just meant for interpolating
    thetas = draw_thetas(10000,rng=np.random.default_rng()) 
    thetas = np.concatenate((thetas, [0.0, 1.0]))
    thetas = np.sort(thetas)
    theta_icdf = interp1d(np.linspace(0, 1, thetas.shape[0]), thetas)
    theta_kde = stats.gaussian_kde(thetas)
    ts = np.linspace(0,1,1024)
    ps = theta_kde(ts) + theta_kde(-ts) + theta_kde(2-ts)
    ptheta = interp1d(ts,ps,bounds_error=False,fill_value = 0.0)
    return ptheta

ptheta = ptheta_func()


## first transformation
## eta, rho, theta, detector frame chirp mass -> 
## -> theta, redshift, detector frame m1 and detector frame m2
def u1(x1,x2,x3,x4):
    prefac = x1/np.power(x2,.6)/2
    return prefac * (1 + np.sqrt(1 - 4*x2))
def u2(x1,x2,x3,x4):
    prefac = x1/np.power(x2,.6)/2
    return prefac * (1 - np.sqrt(1 - 4*x2))
def u3(x1,x2,x3,x4):
    m1 = u1(x1,x2,x3,x4)
    m2 = u2(x1,x2,x3,x4)
    dl = x4*osnr_interp(m1,m2,grid=False)/x3
    try:
        return cosmo_dict['z_at_dL'](dl*1000)
    except ValueError:
        i = np.argmax(dl)
        print(dl[i],m1[i],m2[i],x3[i],x4[i],
              osnr_interp(m1[i],m2[i],grid=False))
        dl = np.where(dl>47.,47.,dl)
        return cosmo_dict['z_at_dL'](dl)
def u4(x1,x2,x3,x4):
    return x4

def v1(y1,y2,y3,y4):
    value = np.power(y1*y2, 3./5)/np.power(y1+y2,1./5)
    return np.where(y1>=y2,value,np.nan)
def v2(y1,y2,y3,y4):
    value = (y1*y2)/np.power(y1+y2,2)
    return np.where(y1>=y2,value,np.nan)
def v3(y1,y2,y3,y4):
    dl = cosmo_dict['dL'](y3) / 1000
    return y4*osnr_interp(y1,y2,grid=False)/dl
def v4(y1,y2,y3,y4):
    return y4

def mass_jacobian(y1,y2,y3,y4):
    eta = v2(y1,y2,y3,y4)
    return ((y1-y2)*eta**0.6)/((y1+y2)**2)
def distance_jacobian(y1,y2,y3,y4):
    dl = cosmo_dict['dL'](y3) / 1000
    ddl_dz = dl/(1+y3) + (1+y3)*cosmo_dict['dH']/1000/cosmo_dict['inv_efunc'](y3)
    return y4*osnr_interp(y1,y2,grid=False)/np.power(dl,2.) * ddl_dz

## second transformation
## redshfit, detector frame m1 and detector frame m2 ->
## -> redshift, q, and source frame m1

def u1_tilde(x1,x2,x3):
    return x1/(1+x3)
def u2_tilde(x1,x2,x3):
    return x2/x1
def u3_tilde(x1,x2,x3):
    return x3
def u4_tilde(x4):
    return x4

def v1_tilde(y1,y2,y3):
    return (1+y3)*y1
def v2_tilde(y1,y2,y3):
    return (1+y3)*y1*y2
def v3_tilde(y1,y2,y3):
    return y3
def v4_tilde(y4):
    return y4

def redshift_jacobian(y1,y2,y3):
    return (1+y3)**2
def q_jacobian(y1,y2,y3):
    return y1

## general utils
def create_dataarray(*samples):
    stacked_samples = np.vstack([samp for samp in samples])
    return xr.DataArray(stacked_samples,dims=['parameter','PE_sample'])
pesummary_key_map = dict(chirp_mass_det='chirp_mass',eta='symmetric_mass_ratio',
                         snr='network_optimal_snr',theta='sky_angle',mass_1_det='mass_1',
                         mass_2_det='mass_2',redshift='redshift',mass_1='mass_1_source',
                         mass_ratio='mass_ratio',luminosity_distance_Mpc='luminosity_distance',
                         mass_2='mass_2_source',prior_m1_q_z='prior',
                         prior='prior',
                        )


## main function
def transform(data, desired_params, desired_prior):
    # check flags
    if desired_params not in ["detectorframeMcetarho", "detectorframem1m2z",
                              "sourceframem1qz", "all"]:
        raise ValueError(
            "parameter options are 'sourceframem1qz','detectorframem1m2z',\
'detectorframeMcetarho', or 'all'")
    if desired_prior not in ['lalprior','original']:
        raise ValueError("prior options are 'lalprior' and 'original'")

    MCHIRP = data.sel(parameter='Mc_det_samps').values
    ETA = data.sel(parameter='eta_samps').values
    RHO = data.sel(parameter='rho_samps').values
    THETA = data.sel(parameter='theta_samps').values

    eta_bounds = (0.0, 0.25)
    mc_bounds =  (0.0, 500.)#(np.min(MCHIRP), np.max(MCHIRP))
    rho_bounds = (0.0, 300.)#(np.min(RHO), np.max(RHO))
    theta_bounds = (0.0, 1.0)

    f = original_prob(MCHIRP, ETA, RHO, THETA,
                      mc_bounds, eta_bounds, rho_bounds, theta_bounds)
    if np.any(f==0):
        f0idx = np.where(f==0)
        print('Warning: some samples outside of prior bounds. ',
              f"mc: {MCHIRP[f0idx]}, ",
              f"eta: {ETA[f0idx].min()}, rho:  ",
              f"{RHO[f0idx].max()}, theta: {THETA[f0idx]}")
    if desired_params == "detectorframeMcetarho":
        new_data = create_dataarray(MCHIRP,ETA,RHO,THETA,f)
        param_names = ['chirp_mass_det','eta','snr','theta','prior']

        if desired_prior == "lalprior":
            raise ValueError(
                "lalprior currently only supported for sourceframem1qz")
    elif desired_params in ["detectorframem1m2z","sourceframem1qz", "all"]:
        M1 = u1(MCHIRP,ETA,RHO,THETA)
        M2 = u2(MCHIRP,ETA,RHO,THETA)
        Z = u3(MCHIRP,ETA,RHO,THETA)

        g = transformed_prob(M1, M2, Z, THETA, mass_jacobian, distance_jacobian,
                             bounds1=mc_bounds, bounds2=eta_bounds,
                             bounds3=rho_bounds, bounds4=theta_bounds)
        if desired_params == "detectorframem1m2z":
           new_data = create_dataarray(M1,M2,Z,THETA,g)
           param_names = ['mass_1_det','mass_2_det','redshift','theta','prior']
           if desired_prior == "lalprior":
               raise ValueError(
                   "lalprior currently only supported for sourceframem1qz")
        else:
            M1S = u1_tilde(M1,M2,Z)
            Q = u2_tilde(M1,M2,Z)
            Z = u3_tilde(M1,M2,Z)
            THETA = u4_tilde(THETA)
            J_tilde = redshift_jacobian(M1S,Q,Z) * q_jacobian(M1S,Q,Z)
            g_tilde = J_tilde * transformed_prob(
                v1_tilde(M1S,Q,Z), v2_tilde(M1S,Q,Z), v3_tilde(M1S,Q,Z),
                v4_tilde(THETA),
                mass_jacobian, distance_jacobian,
                bounds1=mc_bounds, bounds2=eta_bounds, bounds3=rho_bounds,
                bounds4=theta_bounds
            )
            if desired_prior == "original":
                if desired_params =='all':
                    # add some bonus variables that the prior is not written in
                    # terms of, but still might be convenient to have.
                    DL = cosmo_dict['dL'](Z)
                    M2S = M2 / (1+Z)
                    new_data=create_dataarray(MCHIRP,ETA,RHO,THETA,M1,M2,Z,M1S,Q,
                                      DL,M2S,
                                      f,g,g_tilde)
                    param_names =['chirp_mass_det','eta','snr','theta',
                                  'mass_1_det','mass_2_det','redshift',
                                  'mass_1','mass_ratio','luminosity_distance_Mpc','mass_2',
                                  'prior_Mcdet_eta_rho','prior_m1det_m2det_z','prior_m1_q_z']
                else:
                    new_data = create_dataarray(M1S,Q,Z,THETA,g_tilde)
                    param_names = ['mass_1','mass_ratio','redshift','theta','prior']
            else:
                DL = cosmo_dict['dL'](Z)
                ddl_dz = DL/(1+Z) + (1+Z)*cosmo_dict['dH']/cosmo_dict['inv_efunc'](Z)
                ddl_dz /= 1000.
                weights  = ((DL/1000.)**2) * ptheta(THETA)
                weights *= ddl_dz/(mass_jacobian(M1,M2,Z,THETA) *
                                   distance_jacobian(M1,M2,Z,THETA))
                weights /= np.sum(weights)
                idxs = np.random.choice(len(weights), size=len(weights)//17,
                                        p=weights, replace=True)
                RESAMPLED_M1S = M1S[idxs]
                RESAMPLED_Q = Q[idxs]
                RESAMPLED_Z = Z[idxs]
                RESAMPLED_THETA = THETA[idxs]
                new_prior = ((1+RESAMPLED_Z)**2)* lalprior(RESAMPLED_Z) * q_jacobian(
                    RESAMPLED_M1S, RESAMPLED_Q, RESAMPLED_Z) / norm
                if desired_params =='all':
                    M2S = M2 / (1+Z)
                    new_data =create_dataarray(MCHIRP[idxs],ETA[idxs],RHO[idxs],
                                               RESAMPLED_THETA,M1[idxs],M2[idxs],
                                               RESAMPLED_Z,RESAMPLED_M1S,
                                               RESAMPLED_Q,DL[idxs],M2S[idxs],
                                               new_prior)
                    param_names =['chirp_mass_det','eta','snr','theta',
                                  'mass_1_det','mass_2_det','redshift',
                                  'mass_1','mass_ratio',
                                  'luminosity_distance_Mpc','mass_2',
                                  'prior_m1_q_z']

                else:
                    new_data = create_dataarray(RESAMPLED_M1S, RESAMPLED_Q,
                                                RESAMPLED_Z, RESAMPLED_THETA, new_prior)
                    param_names = ['mass_1','mass_ratio','redshift','theta','prior']

    return new_data, param_names
