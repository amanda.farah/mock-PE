import astropy.cosmology as cosmo
import astropy.units as u
import multiprocessing as multi
from pylab import *
from numpy import *
import scipy.interpolate as sip
from scipy.integrate import cumtrapz
import h5py
from GWMockCat import cosmo_utils as cosmo

def draw_thetas(N,rng):
    """Draw `N` random angular factors for the SNR.

    Theta is as defined in [Finn & Chernoff
    (1993)](https://ui.adsabs.harvard.edu/#abs/1993PhRvD..47.2198F/abstract).

    Author: Will Farr
    """

    cos_thetas = rng.uniform(low=-1, high=1, size=N)
    cos_incs = rng.uniform(low=-1, high=1, size=N)
    phis = rng.uniform(low=0, high=2*np.pi, size=N)
    zetas = rng.uniform(low=0, high=2*np.pi, size=N)

    Fps = 0.5*cos(2*zetas)*(1 + square(cos_thetas))*cos(2*phis) - sin(2*zetas)*cos_thetas*sin(2*phis)
    Fxs = 0.5*sin(2*zetas)*(1 + square(cos_thetas))*cos(2*phis) + cos(2*zetas)*cos_thetas*sin(2*phis)

    return np.sqrt(0.25*square(Fps)*square(1 + square(cos_incs)) + square(Fxs)*square(cos_incs))

def next_pow_two(x):
    """Return the next (integer) power of two above `x`.
   
    Author: Will Farr
    """

    x2 = 1
    while x2 < x:
        x2 = x2 << 1
    return x2

def interpolate_optimal_snr_grid(fname = 'optimal_snr_earlyhigh.h5'):
    with h5py.File(fname,'r') as inp:
        ms = array(inp['ms'])
        osnrs = array(inp['SNR'])
        ref_dist_Gpc = inp.attrs['D_L']
    return sip.RectBivariateSpline(ms,ms,osnrs,s=0.0), ref_dist_Gpc

def Pdet_msdet(m1det, m2det, dL, osnr_interp, ref_dist_Gpc = 1.0, dist_unit = u.Mpc, rand_noise = False, thresh=8.0):
    """
    m1det: detector-frame mass 1
    m2det: detector-frame mass 2
    dL: luminosity distance in dist_unit
    osnr_interp: a 2-d spline object constructed by interpolate_optimal_snr_grid
    ref_dist_Gpc: the reference distance at which osnr_interp was calculated
    rand_noise: add random N(0,1)
    """
    dL_Gpc = (dL*dist_unit).to(u.Gpc).value
    if dL_Gpc == 0.0:
        return 1.0
    if rand_noise:
        noise = rs
    else:
        noise = 0.0
    return mean(osnr_interp.ev(m1det,m2det)*ref_dist_Gpc/dL_Gpc*thetas+rs>thresh)

class PdetFromTuple(object):
    def __init__(self, osnr_interp, ref_dist_Gpc, dist_unit, rand_noise, thresh):
        self.osnr_interp = osnr_interp
        self.ref_dist_Gpc = ref_dist_Gpc
        self.dist_unit = dist_unit
        self.rand_noise = rand_noise
        self.thresh = thresh
    def __call__(self, m1m2dL):
        m1, m2, dL = m1m2dL
        return Pdet_msdet(m1, m2, dL, self.osnr_interp, self.ref_dist_Gpc, self.dist_unit, self.rand_noise, self.thresh)

def Pdets_from_masses_distances(m1dets, m2dets, dLs, osnr_interp, ref_dist_Gpc = 1.0, dist_unit = u.Mpc, rand_noise = False, thresh = 8.0):
    """Returns array of Pdets corresponding to the given systems.

    Uses multiprocessing for more efficient computation.
    
    """

    Pdet_tuple = PdetFromTuple(osnr_interp, ref_dist_Gpc, dist_unit, rand_noise, thresh)

    pool = multi.Pool()
    try:
        Pdets = array(pool.map(Pdet_tuple, zip(m1dets, m2dets,dLs)))
    finally:
        pool.close()

    return Pdets

def Pdets_from_masses_redshifts(m1s,m2s,zs,osnr_interp,cosmo_dict,ref_dist_Gpc=1.0, rand_noise = False, thresh=8.0):
    m1dets = m1s*(1.0+zs)
    m2dets = m2s*(1.0+zs)
    dLs = cosmo_dict['dL'](zs)
    dist_unit = cosmo_dict['dist_Mpc']*u.Mpc
    return Pdets_from_masses_distances(m1dets,m2dets,dLs,osnr_interp,ref_dist_Gpc,dist_unit, rand_noise,thresh)


def VT_from_mass(m1,m2,osnr_interp,cosmo_dict, ref_dist_Gpc = 1.0, rand_noise=False,thresh=8.0, g=0.0, time=1.0):
    ''' returns VTs for source-frame masses m1 and m2, g parametrizes the redshift/ distance distribution
    takes in an optimal_snr interpolator and the reference distance ref_dist_Gpc at which the interpolator is defined'''
    ppop_z = cosmo.ppop_z_func(cosmo_dict = cosmo_dict, g = g)
    dL = cosmo_dict['dL']
    dist_unit = cosmo_dict['dist_Mpc']*u.Mpc
    zmin = 0.001
    zmax = 2.0    
    assert Pdet_msdet(m1*(1.0+zmax), m2*(1.0+zmax), dL(zmax), osnr_interp, ref_dist_Gpc, dist_unit, rand_noise, thresh) == 0.0
    assert Pdet_msdet(m1*(1.0+zmin), m2*(1.0+zmin), dL(zmin), osnr_interp, ref_dist_Gpc, dist_unit, rand_noise, thresh) > 0.0
    while zmax - zmin > 1e-3:
        zhalf = 0.5*(zmax+zmin)
        Pdet_half = Pdet_msdet(m1*(1.0+zhalf), m2*(1.0+zhalf), dL(zhalf), osnr_interp, ref_dist_Gpc, dist_unit, rand_noise, thresh)
        if Pdet_half > 0.0:
            zmin = zhalf
        else:
            zmax = zhalf
    def integrand(z):
        return ppop_z(z)*Pdet_msdet(m1*(1+z), m2*(1+z), dL(z), osnr_interp, ref_dist_Gpc, dist_unit, rand_noise,thresh)
    zs_int = linspace(0.0,zmax,32)
    ys_int = array([integrand(z) for z in zs_int])
    return trapz(ys_int, zs_int)*time

class VTFromTuple(object):
    def __init__(self,osnr_interp,cosmo_dict,ref_dist_Gpc,rand_noise,thresh,g,time):
        self.osnr_interp = osnr_interp
        self.cosmo_dict = cosmo_dict
        self.ref_dist_Gpc = ref_dist_Gpc
        self.rand_noise = rand_noise
        self.thresh = thresh
        self.g = g
        self.time = time
    def __call__(self, m1m2):
        m1, m2 = m1m2
        return VT_from_mass(m1,m2, self.osnr_interp, self.cosmo_dict, self.ref_dist_Gpc, self.rand_noise, self.thresh, self.g, self.time)

def VT_from_masses(m1s,m2s,osnr_interp, cosmo_dict, ref_dist_Gpc = 1.0, rand_noise = False, thresh = 8.0, g = 0.0, time = 1.0):
    VT_tuple = VTFromTuple(osnr_interp, cosmo_dict, ref_dist_Gpc, rand_noise, thresh, g, time)
    pool = multi.Pool()
    try:
        VTs = array(pool.map(VT_tuple, zip(m1s,m2s)))
    finally:
        pool.close()
    return VTs
     
def VVmax(m1det, m2det, d, osnr_interp, cosmo_dict, ref_dist_Gpc=1.0, rand_noise=False, thresh = 8.0, g = 0.0):
    z_at_dL = cosmo_dict['z_at_dL']
    dL = cosmo_dict['dL']
    dist_unit = cosmo_dict['dist_Mpc']*u.Mpc
    zdet = z_at_dL(d)
    ppop_z = cosmo.ppop_z_func(cosmo_dict = cosmo_dict, g = g)
    m1, m2 = m1det/(1.0+zdet), m2det/(1.0+zdet)
    zmin = 0.001
    zmax = 2.0
    assert Pdet_msdet(m1*(1.0+zmax), m2*(1.0+zmax), dL(zmax), osnr_interp, ref_dist_Gpc, dist_unit, rand_noise, thresh) == 0.0
    assert Pdet_msdet(m1*(1.0+zmin), m2*(1.0+zmin), dL(zmin), osnr_interp, ref_dist_Gpc, dist_unit, rand_noise, thresh) > 0.0
    while zmax - zmin > 1e-3:
        zhalf = 0.5*(zmax+zmin)
        Pdet_half = Pdet_msdet(m1*(1.0+zhalf), m2*(1.0+zhalf), dL(zhalf), osnr_interp, ref_dist_Gpc, dist_unit, rand_noise, thresh)
        if Pdet_half > 0.0:
            zmin = zhalf
        else:
            zmax = zhalf
    def integrand(z):
        return ppop_z(z)*Pdet_msdet(m1*(1+z), m2*(1+z), dL(z), osnr_interp, ref_dist_Gpc, dist_unit, rand_noise,thresh)
    zs_int = linspace(0.0,maximum(zmax,zdet),32)
    ys_int = array([integrand(z) for z in zs_int])
    cumint = sip.interp1d(zs_int,cumtrapz(ys_int, zs_int, initial=0.0))
    return cumint(zdet)/cumint(zmax)

class VVmaxFromTuple(object):
    def __init__(self,osnr_interp, cosmo_dict, ref_dist_Gpc, rand_noise, thresh, g):
        self.osnr_interp = osnr_interp
        self.cosmo_dict = cosmo_dict
        self.ref_dist_Gpc = ref_dist_Gpc
        self.rand_noise = rand_noise
        self.thresh = thresh
        self.g = g
    def __call__(self,m1m2d):
        m1, m2, d = m1m2d
        return VVmax(m1,m2,d,self.osnr_interp, self.cosmo_dict, self.ref_dist_Gpc, self.rand_noise, self.thresh, self.g)

def VVmax_from_masses_distances(m1dets, m2dets, ds, osnr_interp, cosmo_dict, ref_dist_Gpc = 1.0, rand_noise = False, thresh = 8.0, g = 0.0):
    VVmaxTuple = VVmaxFromTuple(osnr_interp, cosmo_dict, ref_dist_Gpc, rand_noise, thresh, g)
    pool = multi.Pool()
    try:
        VVmaxs = array(pool.map(VVmaxTuple, zip(m1dets,m2dets,ds)))
    finally:
        pool.close()
    return VVmaxs

