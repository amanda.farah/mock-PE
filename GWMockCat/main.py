#!/usr/bin/env python
__author__="Amanda Farah"

from GWMockCat import posterior_utils as mock_PE
from GWMockCat import cosmo_utils
from GWMockCat import vt_utils
from GWMockCat import transforms
from GWMockCat.parser import parser

import numpy as np
from scipy.integrate import cumtrapz
from scipy.interpolate import interp1d
import xarray as xr
import h5py
from gwpopulation.utils import powerlaw
from bilby.hyper.model import Model

from tqdm import tqdm
import os
from importlib import import_module
import inspect

###############################################################
##              Utilities                                    ## 
###############################################################

cosmo_dict = cosmo_utils.interp_cosmology()

def _load_model(model, args, param):
    if param=='mass':
        module = 'gwpopulation.models.mass'
        kwargs = {k :args.hyperparameters[k] for k in
                  ['mmin','mmax']}
    elif param=='redshift':
        module = 'gwpopulation.models.redshift'
        kwargs = dict(z_max=args.max_redshift)
    else:
        raise ValueError('only redshift and mass are supported parameters')
    _model = getattr(import_module(module),model)
    if args.verbose:
        print(f"Using {model} from {module}")

    if inspect.isclass(_model):
        _model = _model(**kwargs)
    return _model


def inverse_transform_sample(pdf, x_limits, rng, N=1000, **pdf_kwargs):
    """Generates N samples from the supplied pdf using inverse transform sampling
    pdf (callable): the probability distribution function defined on x
    x_limits (iterable, length 2): the bounds between which to evaluate the pdf
        and draw samples because many pdfs don't just go to zero by themselves.
    N (int): the number of samples to draw, defaults to 1k,
    pdf_kwargs (kwargs): any keword argument to be passed to the pdf function.
    """

    # get the CDF of the PDF
    xs = np.linspace(x_limits[0], x_limits[1], num=1000)
    f_of_xs = pdf(xs, **pdf_kwargs)
    cdf = cumtrapz(f_of_xs, xs, initial=0)
    cdf /= cdf[-1] # normalize

    # choose random points along that CDF
    y = rng.uniform(size=N)

    # convert those to samples by evaluating the value of x 
    # required to get you that value of the CDF
    interp = interp1d(cdf,xs,kind='quadratic')
    samples = interp(y)

    return samples

def draw_true_from_plaw(mmin,mmax,alpha,beta,lambda_z,num_draws,
                        zmax,rng,return_draw_prob=False):

    ## draw first set of mass samples
    # making mmin a little bigger is a numerical trick to make
    # m2 easily able to not go below mmin
    m1t = inverse_transform_sample(powerlaw,[mmin+0.1,mmax], rng=rng, N=num_draws,
                                   alpha=-alpha,high=mmax,low=mmin)
    # draw mass ratios
    # get close enough with low=mmin/mmax, and then reject ones with m2<mmin,
    # normalize properly in the end
    q = inverse_transform_sample(powerlaw, [mmin/mmax,1], rng=rng, N=num_draws,
                                 alpha=beta, low=mmin/mmax, high=1.)
    # if mass ratio is more assymmetric than allowed by mmin, try again
    invalid = np.asarray(m1t*q <mmin).nonzero()
    num_invalid = len(invalid[0])
    while num_invalid >0:
        more_q = inverse_transform_sample(powerlaw, [mmin/mmax,1], rng=rng, N=num_invalid,
                                          alpha=beta, low=mmin/mmax, high=1.
                                         )
        q[invalid] = more_q
        invalid = np.asarray(m1t*q <mmin).nonzero()
        num_invalid = len(invalid[0])

    ## enforce m1 > m2
    m2t = q * m1t
    ## draw redshift samples
    # g=0 means unif in comov. volumne
    inv_zdist_O3 = cosmo_utils.draw_zs_func(cosmo_dict, zmax=zmax, g=lambda_z)
    zt = inv_zdist_O3(rng.uniform(size=num_draws))

    if return_draw_prob:
        z_prob = cosmo_utils.ppop_z_func(cosmo_dict,lambda_z)(zt)
        zarr = np.linspace(0,zmax,num=1000)
        znorm = np.trapz(cosmo_utils.ppop_z_func(cosmo_dict,lambda_z)(zarr),zarr)
        z_prob /= znorm 
        draw_prob = powerlaw(m1t,-alpha,high=mmax,low=mmin) * powerlaw(q,beta,high=1.,low=mmin/m1t) * z_prob
        return m1t,m2t,zt, draw_prob
    else:
        return m1t, m2t, zt



def main():
    ###############################################################
    ##              Parse Everything                             ## 
    ###############################################################
    args = parser.parse_args()
    # set random seed
    rng = np.random.default_rng(args.random_seed)

    if args.verbose:
        print('Welcome to GWMockCat!')
        print("----------")
        print(parser.format_help())
        print("----------")
        print(parser.format_values())

    # output label/path
    run_label = f"{args.output_path}/{args.run_label}"

    # how many events do we want in the end
    num_events = args.num_events
    num_samples_per_event = args.num_samples_per_event

    # event parameter uncertainties
    uncert_dict = {'threshold_snr': args.threshold_snr,
                   'snr': args.SNR_uncert, 'mc': args.mc_uncert,
                   'Theta':args.Theta_uncert,'eta': args.eta_uncert,
                   'mc_shift':0.0,'mc_coeff':0.0,'mc_power':0.0}
                   #'mc_shift':-0.1,'mc_coeff':0.082,'mc_power':1.94}

    # define cosmology using defaults
    maximum_redshift = args.max_redshift

    # set hyperparam values
    default_hyperparam_values = dict(
        alpha=3.14,
        alpha_1=3.14,
        alpha_2=3.14,
        mmin=4.56,
        delta_m=5.96,
        lam=0.0,
        mpp=35.,
        sigpp=5.,
        mmax=81.08,
        beta=1.70,
        lamb=2.7,
        break_fraction=0.5,
    )
    if args.hyperparameters is None:
        if args.mass_model not in [
            'SinglePeakSmoothedMassDistribution','BrokenPowerLawSmoothedMassDistribution'
        ] or args.redshift_model !='PowerLawRedshift':
            raise ValueError(
                "default hyperparameters only supported for"
                "SinglePeakSmoothedMassDistribution or"
                "BrokenPowerLawSmoothedMassDistribution,"
                "plus PowerLawRedshift"
            )
        else:
            args.hyperparameters = default_hyperparam_values

    # define underlying distribution
    underlying_distribution = Model([
        _load_model(args.mass_model,args,'mass'),
        _load_model(args.redshift_model,args,'redshift')])


    # import optimal snr interpolater
    osnr_interp = transforms.osnr_interp
    ###############################################################
    ##              Generate!                                    ## 
    ###############################################################
    if args.sampling_prior == 'original':
        num_pe_samples = num_samples_per_event
    else:
        num_pe_samples = 17*num_samples_per_event
    for j in tqdm(range(args.num_realizations)):
        observations = 0
        ## have to draw extra since not all will make it through detection
        num_draws = num_events*3000
        while observations < num_events:
            num_draws+=num_events*1000
            underlying_distribution.parameters.update(args.hyperparameters)

            try:
                m1t,m2t,zt,p=draw_true_from_plaw(
                    mmin=args.hyperparameters['mmin'],
                    mmax=args.hyperparameters['mmax'],
                    alpha=args.hyperparameters['alpha'],
                    beta=args.hyperparameters['beta'],
                    lambda_z=args.hyperparameters['lamb'],
                    # draw extra to avoid undersampling
                    num_draws=num_draws*5,
                    zmax=maximum_redshift,
                    rng=rng,
                    return_draw_prob=True
                )
            except KeyError:
                m1t,m2t,zt,p=draw_true_from_plaw(
                    mmin=default_hyperparam_values['mmin'],
                    mmax=default_hyperparam_values['mmax'],
                    alpha=default_hyperparam_values['alpha'],
                    beta=default_hyperparam_values['beta'],
                    lambda_z=default_hyperparam_values['lamb'],
                    # draw extra to avoid undersampling
                    num_draws=num_draws*5,
                    zmax=maximum_redshift,
                    rng=rng,
                    return_draw_prob=True
                )

            # note: p is in terms of m1, q, and z
            mock_data = dict(mass_1=m1t, mass_ratio=m2t/m1t, redshift=zt)
            underlying_distribution.parameters.update(args.hyperparameters)
            weights = underlying_distribution.prob(data=mock_data)
            weights = np.where((weights<0)*(weights>-0.001),0,weights)
            weights /= p

            # resample the mock data using the weights
            idxs=rng.choice(len(m1t),size=num_draws,p=weights/np.sum(weights),replace=False)
            m1t = m1t[idxs]
            m2t = m2t[idxs]
            zt = zt[idxs]
            # create selection effects and measurement uncertainty
            observed,detected_dict = mock_PE.generate_obs_from_true_list(m1t, m2t, zt,
                                                           osnr_interp, cosmo_dict, rng,
                                                           PEsamps=num_pe_samples,
                                                           num_evs=num_events,
                                                           uncert=uncert_dict
                                                          )
            # get rid of caching
            underlying_distribution.parameters.update({'delta_m':0.1, 'lamb':0.1})
            underlying_distribution.prob(dict(mass_1=np.linspace(1,10,num=3),
                                              mass_ratio =np.linspace(0,1,num=3),
                                              redshift=np.linspace(0,1,num=3)
                                             )
                                        )
            # update count
            observations = len(observed)

        ds=xr.Dataset()
        true_vals_ds = xr.Dataset()
        for i in range(num_events):
            transformed_samples,param_names = transforms.transform(observed[f'event_{i}'],
                                                    args.event_params,
                                                    args.sampling_prior
                                                   )
            ds=ds.assign({f'event_{i}':transformed_samples})
            if j < 10:
                true_vals = np.vstack([
                    detected_dict['m1'][i],
                    detected_dict['m2'][i],
                    detected_dict['z'][i],
                    detected_dict['theta'][i],
                    detected_dict['lum_dist'][i],
                    detected_dict['rho_obs'][i],
                ])
                true_vals_ds=true_vals_ds.assign({f'event_{i}':xr.DataArray(true_vals,dims=['parameter','value'])})

        ds=ds.assign_coords(
            {'parameter':param_names,
            'PE_sample':np.arange(num_samples_per_event)
            }
        )
        ds.to_netcdf(f'{run_label}_mock_PE_{j}.nc4')

        if j<10:
            true_vals_ds=true_vals_ds.assign_coords(
                {'parameter':['mass_1','mass_2','redshift','theta','luminosity_distance_Mpc','snr'],
                }
            )
            true_vals_ds.to_netcdf(f'{run_label}_true_values_{j}.nc4')

        if args.pesummary_format:
            if not os.path.exists(run_label):
                os.makedirs(run_label)
            if not os.path.exists(f'{run_label}/realization_{j}'):
                os.makedirs(f'{run_label}/realization_{j}')
            for event in ds:
                evnum = int(event[6:])
                sname = f"S2301{evnum:02}ab"
                with h5py.File(f"{run_label}/realization_{j}/{sname}.h5","w") as ff:
                    group_pe = ff.create_group("MDC/posterior_samples")
                    group_true = ff.create_group("MDC/injected_values")
                    for param in ds.parameter.values:
                        group_pe.create_dataset(transforms.pesummary_key_map[param],
                                             data=ds[event].sel(parameter=param).values)
                    if j<10:
                        for param in true_vals_ds.parameter.values:
                            group_true.create_dataset(transforms.pesummary_key_map[param],
                                                 data=true_vals_ds[event].sel(parameter=param).values)


    if args.create_injs:
        if args.verbose:
            print('Generating injections')
            print("----------")
        m1i,m2i,zi,pdraw = draw_true_from_plaw(args.inj_mmin,
                                               args.inj_mmax,
                                               args.inj_alpha,
                                               args.inj_beta,
                                               args.inj_lamb,
                                               num_draws=int(args.num_injections),
                                               return_draw_prob=True,
                                               zmax=maximum_redshift+1.,
                                               rng=rng,
                                              )
        pdraw /= m1i # jacobian to go from q to m2
        m1idet, m2idet = m1i*(1+zi), m2i*(1+zi)
        dli = cosmo_dict['dL'](zi) * cosmo_dict['dist_Mpc']/1000. # in Gpc
        thetas = vt_utils.draw_thetas(len(m1i),rng=rng)
        SNR_true = osnr_interp(m1idet, m2idet, grid=False)/dli * thetas
        SNR_obs = SNR_true + uncert_dict['snr'] * rng.normal(size=len(m1i))

        # Format adaped from 
        # https://git.ligo.org/reed.essick/o1-o2-semianalytic-and-o3-real-injections/-/blob/main/create-multi-run-mixture#L159
        with h5py.File(f"{run_label}_injections.h5","w") as ff:
            group = ff.create_group("injections")

            group.create_dataset("mass1_source", data=m1i)
            group.create_dataset("mass2_source", data=m2i)
            group.create_dataset("redshift", data=zi)
            group.create_dataset("sampling_pdf", data=pdraw)
            group.create_dataset("optimal_snr_l", data=SNR_obs)# use single-ifo SNR

            ff.attrs.create("total_generated", len(m1i))
            ff.attrs.create("analysis_time_s", 28749576) # taken from endo3_bbhpop-LIGO-T2100113-v12.hdf5 
            group.attrs.create("total_generated", len(m1i))
            group.attrs.create("analysis_time_s", 28749576)
