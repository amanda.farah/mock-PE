__author__ = "Maya Fishbach"
import lalsimulation as ls
import lal
import numpy as np
import h5py
from astropy import units as u
from GWMockCat.vt_utils import next_pow_two
from tqdm import tqdm


def optimal_snr_detframe(m1_detector, m2_detector, dL, psd_fn=ls.SimNoisePSDaLIGOEarlyHighSensitivityP1200087,psd_file = None, dist_unit = u.Mpc, a1z = 0.0, a2z = 0.0):
    """Return the optimal SNR of a signal.

        :param m1_detector: The detector-frame mass 1 in Msun.

        :param m2_detector: The detector-frame mass 2 in Msun.

        :param dL: The distance in units dist_unit

        :param psd_fn: A function that returns the detector PSD at a given
        frequency (default is early aLIGO high sensitivity, defined in
        [P1200087](https://dcc.ligo.org/LIGO-P1200087/public).

        :param psd_file: A txt file containing frequency in column 1, PSD in column 2. Optional and only used if psd_fn is unused.

        :param dis_unit: The distance unit used

        :return: The SNR of a face-on, overhead source.

        Author: based on Will Farr's code
        """

    # Get dL, Gpc
    dL = (dL*dist_unit).to(u.Gpc).value

    # Basic setup: min frequency for w.f., PSD start freq, etc.
    fmin = 19.0
    fref = 40.0
    psdstart = 20.0

    # This is a conservative estimate of the chirp time + MR time (2 seconds)
    tmax = ls.SimInspiralChirpTimeBound(fmin, m1_detector*lal.MSUN_SI, m2_detector*lal.MSUN_SI, a1z, a2z) + 2

    df = 1.0/next_pow_two(tmax)
    fmax = 2048.0 # Hz --- based on max freq of 5-5 inspiral

    # Generate the waveform, redshifted as we would see it in the detector, but with zero angles (i.e. phase = 0, inclination = 0)
    hp, hc = ls.SimInspiralChooseFDWaveform(m1_detector*lal.MSUN_SI, m2_detector*lal.MSUN_SI, 0.0, 0.0, a1z, 0.0, 0.0, a2z, dL*1e9*lal.PC_SI, 0.0, 0.0, 0.0, 0.0, 0.0, df, fmin, fmax, fref, None, ls.IMRPhenomPv2)

    Nf = int(round(fmax/df)) + 1
    fs = np.linspace(0, fmax, Nf)
    sel = fs > psdstart

    # PSD
    sffs = lal.CreateREAL8FrequencySeries("psds", 0, 0.0, df, lal.HertzUnit, fs.shape[0])
    if  psd_file is None:
        psd_fn(sffs, psdstart)
        return ls.MeasureSNRFD(hp, sffs, psdstart, -1.0) #http://software.ligo.org/docs/lalsuite/lalsimulation/group___l_a_l_sim_utils__h.html#gabecc316af1ce5186a981fae2a447a194
    else:
        sffs = lal.CreateREAL8FrequencySeries("psds", 0, 0.0, df, lal.HertzUnit, fs.shape[0])
        ls.SimNoisePSDFromFile(sffs,psdstart,psd_file) #expects ASD file, so take square root
#        sffs.data.data = sffs.data.data**0.5
        return ls.MeasureSNRFD(hp, sffs, psdstart, -1.0)

def optimal_snr_grid(mmin, mmax, dL = 1000.0, dist_unit = u.Mpc, nm = 100, psd_fn=ls.SimNoisePSDaLIGOEarlyHighSensitivityP1200087,psd_file = None, a1z = 0.0, a2z = 0.0):
    ms = np.exp(np.linspace(np.log(mmin), np.log(mmax), nm))
    osnrs = np.zeros((nm, nm))
    for i, m1 in tqdm(enumerate(ms)):
        for j in range(i+1):
            m2 = ms[j]
            osnrs[i,j] = optimal_snr_detframe(m1, m2, dL, psd_fn, psd_file, dist_unit, a1z, a2z)
            osnrs[j,i] = osnrs[i,j]
    return ms, osnrs

def save_optimal_snr_grid(mmin, mmax, dL = 1000.0, dist_unit = u.Mpc, nm = 100, psd_fn = ls.SimNoisePSDaLIGOEarlyHighSensitivityP1200087,psd_file = None, outname = 'optimal_snr_earlyhigh.h5', a1z = 0.0, a2z = 0.0):
    ms, osnrs = optimal_snr_grid(mmin, mmax, dL, dist_unit, nm, psd_fn, psd_file, a1z, a2z)
    dL_Gpc = (dL*dist_unit).to(u.Gpc).value
    with h5py.File(outname, 'w') as out:
        out.create_dataset('ms', data=ms, compression='gzip', shuffle=True)
        out.create_dataset('SNR', data=osnrs, compression='gzip', shuffle=True)
        out.attrs['D_L'] = '%.6f Gpc' %(dL_Gpc)

save_optimal_snr_grid(0.05,150,nm=100,psd_file='/Users/amandafarah/proj/useful_scripts/mock-PE/sensitivity/et_d.txt',outname='/Users/amandafarah/proj/useful_scripts/mock-PE/sensitivity/optimal_snr_ET_design.h5')
#save_optimal_snr_grid(5,200,nm=100,outname='test_psdfn.h5',psd_fn=ls.SimNoisePSDaLIGOaLIGOO3LowT1800545)
