`h5` files were generated using `create_snr_grid.py` and modifying the last line.

You will need to install `lalsimulation` and `lal` to run `create_snr_grid.py`. 

`.txt` files are PSD files to feed into `create_snr_grid.optimal_snr_detframe()`.
`aligo_O4high.txt`, `aligo_O4low.txt`, `aligo_O3_actual_L1.txt`, `aligo_O3_actual_H1.txt`, and `AplusDesign_O5.txt`` were downloaded from [dcc.ligo.org/LIGO-T2000012-v2](dcc.ligo.org/LIGO-T2000012-v2).
